<head>
    <meta charset="utf-8"/>

    <title>Webback4</title>
    <link rel="stylesheet" media="all" href="style.css"/>
    <style>
        .error {
            border: 2px solid red;
        }
        .error-radio
        {
            border: 2px solid red;
            width: 150px;
        }
        .error-bio
        {
            border: 2px solid red;
            width: 250px;
            height: 16px;
        }
        .error-check
        {
            border: 2px solid red;
            width: 350px;
        }
        .error-abil
        {
            border: 2px solid red;
            width: 290px;
            height: 70px;
        }
        .error-radio-limb
        {
            border: 2px solid red;
            width: 250px;
            height:35px;
        }
    </style>
</head>
<body>
<?php
if (!empty($messages)) {
    print('<div id="messages">');
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}
?>

<form action="index.php" id="my-formcarry" accept-charset="UTF-8" class="main" method="POST">
    <input style="margin-bottom : 1em" id="formname" type="text" name="fio" placeholder="Введите имя"
        <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>">
    <input style="margin-bottom : 1em;margin-top : 1em" id="formmail" type="email" name="email"
           placeholder="Введите почту"
        <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"
        >
    <label >
        Год рождения: <br/>
        <input <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year_value']; ?> id="dr" name="birthyear"
               value=""
               type="number"/>
    </label><br/>

    Выберите пол:<br/>
    <label <?php if($errors['sex']){print 'class="error-radio"';}?>>
        <input <?php if($values['sex_value']=="man"){print 'checked';}?> type="radio"
                  name="radio2" value="man"/>
        Мужской</label>
    <label <?php if($errors['sex']){print 'class="error-radio"';}?>> <input <?php if($values['sex_value']=="woman"){print 'checked';}?> type="radio"
                  name="radio2" value="woman"/>
        Женский</label>
    <br/>
   <div <?php if($errors['limb']){print 'class="error-radio-limb"';}?>>
    Количество конечностей: <br/>
    <label><input
            <?php if($values['limb_value']=="0"){print 'checked';}?>
                type="radio"
                  name="radio1" value="0"/>
        0</label>
    <label><input
            <?php if($values['limb_value']=="1"){print 'checked';}?>
                type="radio"
                  name="radio1" value="1"/>
        1</label>
    <label><input
            <?php if($values['limb_value']=="2"){print 'checked';}?>
                type="radio"
                  name="radio1" value="2"/>
        2</label>
    <label><input
            <?php if($values['limb_value']=="3"){print 'checked';}?>
                type="radio"
                  name="radio1" value="3"/>
        3</label>
    <label><input
            <?php if($values['limb_value']=="4"){print 'checked';}?>
                type="radio"
                  name="radio1" value="4"/>
        4</label>
    <label><input
            <?php if($values['limb_value']=="5"){print 'checked';}?>
                type="radio"
                  name="radio1" value="5"/>
        5</label><br>
   </div>
    <label>
        Сверхспособности:
        <br/>
        <div <?php if ($errors['abil']) {print 'class="error-abil"';} ?>> <select id="sp" name="select1[]"
                multiple="multiple">
            <option <?php if($values['abil_value']=="1"){print 'selected';}?> value="1">Бессмертие</option>
            <option <?php if($values['abil_value']=="1"){print 'selected';}?> value="1">Прохожение сквозь стены</option>
            <option <?php if($values['abil_value']=="1"){print 'selected';}?> value="1">Левитация</option>
            <option <?php if($values['abil_value']=="1"){print 'selected';}?> value="1">Дискретка на 5</option>
            <option <?php if($values['abil_value']=="1"){print 'selected';}?> value="1">Диффуры на 5</option>
                <option <?php if($values['abil_value']=="1"){print 'selected';}?> value="1">Нету</option>
        </select> </div>
    </label><br/>

    <label <?php if ($errors['bio']) {print 'class="error-bio"';} ?>>
        Биография: <br/>
        <textarea id="biog" name="textarea1"><?php print $values['bio_value'];?></textarea>
    </label><br/>

    <div <?php if ($errors['check']) {print 'class="error-check"';} ?>><label><input <?php if($values['check_value']=="1"){print 'checked';}?> style="margin-bottom : 1em;margin-top : 1em;" id="formcheck" type="checkbox" name="checkbox"
                                                                                 value="1">Согласие на обработку персональных данных</label></div>

    <input type="submit" style="margin-bottom : -1em" id="formsend" class="buttonform" value="Отправить">
</form>
</body>
