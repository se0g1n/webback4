<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();

    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';
    }

    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email']=!empty($_COOKIE['email_error']);
    $errors['sex']=!empty($_COOKIE['sex_error']);
    $errors['bio']=!empty($_COOKIE['bio_error']);
    $errors['check']=!empty($_COOKIE['check_error']);
    $errors['abil']=!empty($_COOKIE['abil_error']);
    $errors['year']=!empty($_COOKIE['year_error']);
    $errors['limb']=!empty($_COOKIE['limb_error']);

    if ($errors['fio']) {
        setcookie('fio_error', '', 100000);
        $messages[] = '<div>Заполните имя корректно.</div>';
    }
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div>Заполните почту</div>';
    }
    if ($errors['sex']) {
        setcookie('sex_error', '', 100000);
        $messages[] = '<div>Выберите пол</div>';
    }
    if ($errors['bio']) {
        setcookie('bio_error', '', 100000);
        $messages[] = '<div>Введите биографию</div>';
    }
    if ($errors['check']) {
        setcookie('check_error', '', 100000);
        $messages[] = '<div>Подтвердите согласие</div>';
    }
    if ($errors['abil']) {
        setcookie('abil_error', '', 100000);
        $messages[] = '<div>Выберите сверхспособность</div>';
    }
    if ($errors['year']) {
        setcookie('year_error', '', 100000);
        $messages[] = '<div>Корректно введите дату рождения</div>';
    }
    if ($errors['limb']) {
        setcookie('limb_error', '', 100000);
        $messages[] = '<div>Выберите количество конечностей</div>';
    }

    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['sex_value'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
    $values['bio_value'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
    $values['check_value'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];
    $values['abil_value'] = empty($_COOKIE['abil_value']) ? '' : $_COOKIE['abil_value'];
    $values['year_value'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
    $values['limb_value'] = empty($_COOKIE['limb_value']) ? '' : $_COOKIE['limb_value'];
    include('form.php');
}
else {
    $errors = FALSE;
    if (empty($_POST['fio'])|| (preg_match("/^[a-z0-9_-]{2,20}$/i", $_POST['fio']))) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['radio2'])) {
        setcookie('sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('sex_value', $_POST['radio2'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['textarea1'])) {
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('bio_value', $_POST['textarea1'], time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['checkbox'])) {
        setcookie('check_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('check_value', $_POST['checkbox'], time() + 365 * 24 * 60 * 60);
    }
    $kek=0;
    $myselect=$_POST['select1'];
    for($i=0;$i<5;$i++)
    {
        if($myselect[$i]!=1)
        {
            $myselect[$i]=0;
        }
        else
            $kek=1;
    }
    if (!$kek) {
        setcookie('abil_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('abil_value', $_POST['select1'], time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['radio1'])) {
        setcookie('limb_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('limb_value', $_POST['radio1'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['birthyear'])||$_POST['birthyear']<1886 || $_POST['birthyear']>2021) {
        setcookie('year_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('year_value', $_POST['birthyear'], time() + 365 * 24 * 60 * 60);
    }


    if ($errors) {
        header('Location: index.php');
        exit();
    } else {
        setcookie('fio_error', '', 100000);
        setcookie('email_error','',100000);
        setcookie('sex_error','',100000);
        setcookie('bio_error','',100000);
        setcookie('check_error','',100000);
        setcookie('abil_error','',100000);
        setcookie('year_error','',100000);
        setcookie('limb_error','',100000);
    }

    $user = 'u20986';
    $pass = '3457435';
    $db = new PDO('mysql:host=localhost;dbname=u20986', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

    $stmt = $db->prepare("INSERT INTO form (name, year,email,sex,limb,bio,checkbox) VALUES (:fio, :birthyear,:email,:sex,:limb,:bio,:checkbox)");
    $stmt -> execute(array('fio'=>$_POST['fio'], 'birthyear'=>$_POST['birthyear'],'email'=>$_POST['email'],'sex'=>$_POST['radio2'],'limb'=>$_POST['radio1'],'bio'=>$_POST['textarea1'],'checkbox'=>$_POST['checkbox']));

    $stmt=$db->prepare("INSERT INTO all_abilities(ability_god,ability_through_walls,ability_levity,ability_kostenko,ability_kolotiy) VALUES(:god,:wall,:levity,:kostenko,:kolotiy)");

    $stmt->execute(array('god'=>$myselect[0],'wall'=>$myselect[1],'levity'=>$myselect[2],'kostenko'=>$myselect[3],'kolotiy'=>$myselect[4]));

    setcookie('save', '1');
    header('Location: index.php');
}
